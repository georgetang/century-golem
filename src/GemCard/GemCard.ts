import { GemSet } from "../GemSet"

type GemCardTransform = {
    before: GemSet,
    after: GemSet,
}

type GemCardUpgrade = {
    upgrade: GemSet,
}

export type GemCard = (GemCardTransform | GemCardUpgrade ) & { used?: boolean, field: boolean};

export function playGemCard(gemCard: GemCard, gemSet: GemSet): GemSet{
    const newGemSet = {...gemSet};
    let k: keyof typeof newGemSet;

    if("before" in gemCard){
        for(k in newGemSet){
            newGemSet[k] -= gemCard.before[k];
            newGemSet[k] += gemCard.after[k];
        }

    } else if("upgrade" in gemCard){
        newGemSet.yellow -= gemCard.upgrade.yellow;
        newGemSet.green += gemCard.upgrade.yellow;
        newGemSet.green -= gemCard.upgrade.green;
        newGemSet.blue += gemCard.upgrade.green;
        newGemSet.blue -= gemCard.upgrade.blue;
        newGemSet.pink += gemCard.upgrade.blue;
    }

    return newGemSet;
}