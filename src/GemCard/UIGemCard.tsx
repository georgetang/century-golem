import { GemCard } from "./GemCard";
import "./GemCard.css";

const gemCard = (props: GemCard & {onInteract: () => void}) => {
    let info;
    const interactText = props.field ? "pick" : (!props.used ? "use" : null);

    if("before" in props){
        info = <div className="info">
            <ul>
                <li>{props.before.yellow}</li>
                <li>{props.before.green}</li>
                <li>{props.before.blue}</li>
                <li>{props.before.pink}</li>
            </ul>
            <ul>
                <li>{props.after.yellow}</li>
                <li>{props.after.green}</li>
                <li>{props.after.blue}</li>
                <li>{props.after.pink}</li>
            </ul>
        </div>
    } else if("upgrade" in props){
        info = <div className="info">
            <ul>
                <li>{props.upgrade.yellow}</li>
                <li>{props.upgrade.green}</li>
                <li>{props.upgrade.blue}</li>
                <li>{props.upgrade.pink}</li>
            </ul>
        </div>
    } 

    return <div className="gemCard">
        {info}

        
        {(props.field || !props.used) ? <button onClick={props.onInteract}>{interactText}</button> : null}
    </div>
}

export default gemCard;