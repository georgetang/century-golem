import { GemSet } from "../GemSet";

export type Golem = {
    points: number,
    gemSet: GemSet
}