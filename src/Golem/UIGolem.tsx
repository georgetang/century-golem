import { Golem } from "./Golem";
import "./Golem.css";

const UIGolem = (props: Golem & {
    onInteract: () => void,
    canBuy: boolean
}) =>
<div className="golem">
    <div className="info">
        <ul>
            <li>{props.gemSet.yellow}</li>
            <li>{props.gemSet.green}</li>
            <li>{props.gemSet.blue}</li>
            <li>{props.gemSet.pink}</li>
        </ul>

        <div>
            points: {props.points}
        </div>
    </div>
    {props.canBuy ? <button onClick={props.onInteract}>buy</button> : null}
</div>


export default UIGolem;