export type GemSet = {
    yellow: number,
    green: number,
    blue: number,
    pink: number
}