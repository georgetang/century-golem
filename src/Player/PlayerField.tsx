import { GemCard, GemCard as GemCardProps } from "../GemCard/GemCard";
import UIGemCard from "../GemCard/UIGemCard";
import { Player } from "./Player";
import './PlayerField.css';

const PlayerField = (props: Player & { 
    onGemCardInteract: (gemCard: GemCard, index: number) => void,
    onRest: () => void,
    canRest: boolean
}) => 
<div className="playerField">
    <ul className="info">
        <li>{props.name}</li>
        <li>points: {props.points}</li>
        <li>y: {props.gemSet.yellow}</li>
        <li>g: {props.gemSet.green}</li>
        <li>b: {props.gemSet.blue}</li>
        <li>p: {props.gemSet.pink}</li>
    </ul>

    <div className="gemCards">
        {props.gemCards.map((gemCard, index) => {
            let GemCard;

            GemCard= <UIGemCard
                {...gemCard}
                onInteract={() => props.onGemCardInteract(gemCard, index)}
            />

            return GemCard;
        })}

        {props.canRest ? <button onClick={props.onRest}>Rest</button> : null}
    </div>
</div>

export default PlayerField;