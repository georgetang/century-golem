import { GemCard } from "../GemCard/GemCard";
import { GemSet } from "../GemSet";
import { Golem } from "../Golem/Golem";

export type Player = {
    name: string,
    points: number,
    gemSet: GemSet,
    golems: Golem[],
    gemCards: GemCard[]
}