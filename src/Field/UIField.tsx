import { Field } from "./Field";
import "./Field.css";
import UIGemCard from '../GemCard/UIGemCard';
import { GemCard } from "../GemCard/GemCard";
import UIGolem from "../Golem/UIGolem";
import { Golem } from "../Golem/Golem";
import { GemSet } from "../GemSet";

const UIField = (props: Field & {
    gemSet: GemSet,
    onGemCardInteract: (gemCard: GemCard, index: number) => void,
    onGolemCardInteract: (golem: Golem, index: number) => void
}) =>
<div id="field">
    <div id="golems">
        {props.golemDeck.slice(0, 5).map((golem, index) => {
            let canBuy = true;
            let k: keyof typeof props.gemSet;
            for(k in props.gemSet){
                if(props.gemSet[k] < golem.gemSet[k])
                    canBuy = false;
            }

            return <UIGolem
                {...golem}
                canBuy={canBuy}
                onInteract={() => props.onGolemCardInteract(golem, index)}
            />
        })}
    </div>
    <div id="gemCards">
        {props.gemCardDeck.slice(0, 6).map((gemCard, index) => <UIGemCard
            {...gemCard}
            onInteract={() => props.onGemCardInteract(gemCard, index)}
        />)}
    </div>
</div>


export default UIField;