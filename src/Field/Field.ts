import { GemCard } from "../GemCard/GemCard";
import { Golem } from "../Golem/Golem";

export type Field = {
    golemDeck: Golem[],
    gemCardDeck: GemCard[]
}