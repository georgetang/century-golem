import './App.css';
import { GemCard, playGemCard } from './GemCard/GemCard';
import { GemSet } from './GemSet';
import { Golem } from './Golem/Golem';
import PlayerField from './Player/PlayerField';
import { Player } from './Player/Player';
import Field from './Field/UIField';
import allGolems from'./golems.json';
import allGemCards from './gemCards.json';
import { useState } from 'react';

const DEFAULT_GEM_CARDS: GemCard[] = [
  {
    before: {yellow: 0, green: 0, blue: 0, pink: 0},
    after: {yellow: 2, green: 0, blue: 0, pink: 0},
    field: false
  },
  {
    before: {yellow: -1, green: 0, blue: 0, pink: 0},
    after: {yellow: 0, green: 0, blue: 0, pink: 0},
    field: false
  }
]

function App() {
  const [golemDeck, setGolemDeck] = useState(allGolems);
  const [gemCardDeck, setGemCardDeck] = useState(allGemCards);

  const [player , setPlayer] = useState({
    name: "Player 1",
    points: 0,
    gemSet: {yellow: 3, green: 0, blue: 0, pink: 0},
    golems: golemDeck,
    gemCards: DEFAULT_GEM_CARDS
  });

  function onPlayGemCard(gemCard: GemCard, index: number){
    const newPlayer = {...player};
    const newGemSet = playGemCard(gemCard, newPlayer.gemSet);
    newPlayer.gemSet = newGemSet;
    newPlayer.gemCards[index].used = true;
    setPlayer(newPlayer);
  }

  function onPickGemCard(gemCard: GemCard, index: number){
    const newPlayer = {...player};
    const newGemCardDeck = [...gemCardDeck];
    newGemCardDeck[index].field = false;
    newPlayer.gemCards.push(gemCard);
    newGemCardDeck.splice(index, 1);

    setPlayer(newPlayer);
    setGemCardDeck(newGemCardDeck);
  }

  function onBuyGolem(golem: Golem, index: number){
    const newPlayer = {...player};
    const newGolemDeck = [...golemDeck];
    
    newPlayer.golems.push(golem);
    newPlayer.points += golem.points;

    let k: keyof typeof newPlayer.gemSet;
    for(k in newPlayer.gemSet){
      newPlayer.gemSet[k] -= golem.gemSet[k];
    }

    newGolemDeck.splice(index, 1);

    setPlayer(newPlayer);
    setGolemDeck(newGolemDeck);
  }

  function onRest(){
    const newPlayer = {...player};
    newPlayer.gemCards.forEach((gemCard, index, gemCards) => {
      gemCards[index].used = false;
    });
    setPlayer(newPlayer);
  }

  let canRest = false;
  player.gemCards.forEach(gemCard => {
    if(gemCard.used)
      canRest= true;
  });

  return <div className="App">
    <Field
      gemSet={player.gemSet}
      gemCardDeck={gemCardDeck}
      golemDeck={golemDeck}
      onGemCardInteract={onPickGemCard}
      onGolemCardInteract={onBuyGolem}
    />
    <PlayerField 
      {...player}
      onRest={onRest}
      canRest={canRest}
      onGemCardInteract={onPlayGemCard}
    />
  </div>
}

export default App;
